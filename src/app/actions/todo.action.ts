export class AddTodo {
  static readonly type = '[TODO SECTION] AddTodo';
  constructor(
    public payload: {
      title: string;
      description: string;
      id: any;
    }
  ) {}
}
