import { Component, OnInit } from '@angular/core';
import { Select, Selector, Store } from '@ngxs/store';
import { Observable, tap } from 'rxjs';
import { TodoState, TodoStateModel } from './state/todoState.state';
import { AddTodo } from './actions/todo.action';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private store: Store) {}
  title: string = '';
  description: string = '';
  state$: Observable<any> = new Observable();
  ngOnInit(): void {
    this.state$ = this.store.select(TodoState);
  }
  addToStore() {
    this.store.dispatch(
      new AddTodo({
        title: this.title,
        description: this.description,
        id: Math.random(),
      })
    );
  }
}
