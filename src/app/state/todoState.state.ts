import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { AddTodo } from '../actions/todo.action';
export interface TodoStateModel {
  todo: any[];
}
@State<TodoStateModel>({
  name: 'todoState',
  defaults: { todo: [] },
})
@Injectable()
export class TodoState {
    
  @Action(AddTodo)
  addTodo(ctx: StateContext<TodoStateModel>, { payload }: AddTodo) {
    const state = ctx.getState();

    ctx.patchState({ todo: [...state.todo, payload] });
  }
}
